---
- name: Make key for {{ ec2_name }}
  ec2_key:
    name: "{{ ec2_name }}"
    aws_access_key: "{{ AWS_ACCESS_KEY_ID }}"
    aws_secret_key: "{{ AWS_SECRET_ACCESS_KEY }}"
    ec2_region: "{{ ec2_region }}"
  register: ec2_key_result

- name: Save private key to local
  copy:
    content: "{{ ec2_key_result.key.private_key }}"
    dest: ~/.ssh/{{ ec2_name }}.pem
    mode: 0600
  when: ec2_key_result.changed

- name: Discover {{ ansible_hostname }} public IP 
  ipify_facts:
    api_url: https://api.ipify.org/
  register: ipify_public_ip

- name: Create security group 
  ec2_group:
    aws_access_key: "{{ AWS_ACCESS_KEY_ID }}"
    aws_secret_key: "{{ AWS_SECRET_ACCESS_KEY }}"
    region: "{{ ec2_region }}"
    name: "{{ ec2_name }}"
    description: "{{ ec2_name }}"
    rules:
      - proto: tcp
        from_port: 22
        to_port: 22
        cidr_ip: "{{ ansible_facts.ipify_public_ip }}/32"
        rule_desc: "{{ ansible_hostname }}"
      - proto: tcp
        from_port: 80
        to_port: 80
        cidr_ip: 0.0.0.0/0
        rule_desc: allow all on port 80
      - proto: tcp
        from_port: 443
        to_port: 443
        cidr_ip: 0.0.0.0/0
        rule_desc: allow all on port 443
    purge_rules: yes
    purge_rules_egress: no
    tags:
      Name: "{{ ec2_name }}"
      Stack: "{{ stack }}"
      Project: "{{ project }}"
  register: security_grp

- name: Create bioviz-connect EC2 
  ec2:
    aws_access_key: "{{ AWS_ACCESS_KEY_ID }}"
    aws_secret_key: "{{ AWS_SECRET_ACCESS_KEY }}"
    ec2_region: "{{ ec2_region }}"
    instance_type: "{{ ec2_instance_type }}"
    keypair: "{{ ec2_name }}"
    image: "{{ ec2_image_name }}"
    assign_public_ip: yes
    vpc_subnet_id: "{{ vpc_subnet_id }}"
    group: "{{ ec2_name }}"
    wait: true
    exact_count: 1 
    count_tag:
      Name: "{{ ec2_name }}"
    instance_tags: 
      Name: "{{ ec2_name }}" 
      Stack: "{{ stack }}"
      Project: "{{ project }}"
  register: ec2

- name: Assign public IP
  ec2_eip:
    aws_access_key: "{{ AWS_ACCESS_KEY_ID }}" 
    aws_secret_key: "{{ AWS_SECRET_ACCESS_KEY }}" 
    ec2_region: "{{ ec2_region }}"
    device_id: "{{ ec2.tagged_instances[0].id }}"
    in_vpc: true # https://github.com/ansible-collections/community.aws/issues/374
  register: eip

- name: Add tags to the elastic ip
  ec2_tag:
    aws_access_key: "{{ AWS_ACCESS_KEY_ID }}" 
    aws_secret_key: "{{ AWS_SECRET_ACCESS_KEY }}" 
    ec2_region: "{{ ec2_region }}"
    resource: "{{ eip.allocation_id }}"
    tags:
      Name: "{{ ec2_name }}"
      Stack: "{{ stack }}"
      Project: "{{ project }}"

- name: Wait for EC2 to start up
  wait_for:
    host: "{{ eip.public_ip }}"
    port: 22
    state: started
    connect_timeout: 100

- name: Create in-memory host group for ansible to interact with
  add_host:
    hostname: "{{ eip.public_ip }}"
    groupname: ec2
    ansible_ssh_common_args: "-o StrictHostKeyChecking=no"
    stdout_callback: yaml
    ansible_ssh_user: ubuntu
    ansible_ssh_private_key_file: ~/.ssh/{{ ec2_name }}.pem
  changed_when: False

- debug:
    msg: EC2 {{ ec2_name }} set up with IP {{ eip.public_ip }}

...
